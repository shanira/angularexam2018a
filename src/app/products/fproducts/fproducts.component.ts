import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';

@Component({
  selector: 'fproducts',
  templateUrl: './fproducts.component.html',
  styleUrls: ['./fproducts.component.css']
})
export class FproductsComponent implements OnInit {

    products;
  
    //only indepense injection
    constructor(private service:ProductsService) { }
  
    //רוצים למשוך את הרשימה שהקומפוננט נוצר
    //פונקציה שמתעוררת ברגע שהקומפוננט נוצר לאחר הקונסטרקטור
    ngOnInit() {
      this.service.getProductFire().subscribe(response=>{
        console.log(response);
        this.products = response;
      });
    }
  
  }
  
