import { Component, OnInit } from '@angular/core';
import { ProductsService } from './products.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router,ParamMap, Routes  } from '@angular/router';

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements OnInit {
  searchform = new FormGroup({
    name: new FormControl('',),
    price: new FormControl('',)
  });
  products;
  productsKeys;
  constructor(private service:ProductsService, private router: Router) {
    service.getProducts().subscribe(response=>{
      this.products = response.json();
      this.productsKeys = Object.keys(this.products);
    });

   }
  sendData(){
    if(this.searchform.invalid) return;
    this.service.getProduct(this.searchform.value).subscribe(response =>{
      console.log(response);
      this.service.getProducts().subscribe(response => {
        this.products =  response.json();
        this.productsKeys = Object.keys(this.products);
        this.router.navigate(['/search-result']);

      });      
    });
  }
  updateUser(id){
        this.service.getProduct(id).subscribe(response=>{
          this.products = response.json();      
      }); 
    }  

  ngOnInit() {
  }

}


