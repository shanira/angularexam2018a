import { Injectable } from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {Http, Headers} from '@angular/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { environment } from './../../environments/environment';

@Injectable()
export class ProductsService {

  http:Http;
  getProducts(){
    //get messages from the SLIM rest API (Don't say DB)
    //return  this.http.get('http://localhost/angular/slim/products');
    return  this.http.get(environment.url+'products');
    }
  
  getProduct(key){  
    //return this.http.get('http://localhost/slimexampletest/users/'+key);
    //return this.http.get('http://localhost/angular/slim/products/'+ key);
    return this.http.get(environment.url+'products/'+ key);
 }
  postProduct(data){
    //המרת גייסון למפתח וערך
    let options = {
    headers: new Headers({
    'content-type':'application/x-www-form-urlencoded'
    })
    }
    let params = new HttpParams().append('name',data.name).append('phone',data.price);      
    //return this.http.post('http://localhost/angular/slim/products', params.toString(), options);
    return this.http.post(environment.url+'products', params.toString(), options);
  
    }

  updateProduct(data){       
  let options = {
    headers: new Headers({
      'content-type':'application/x-www-form-urlencoded'
    })   
  };

  let params = new HttpParams().append('name',data.name).append('price',data.price);  

  return this.http.put(environment.url+'products/'+ data.id,params.toString(), options);      
}

    //השרת שמתחבר לפיירבייס
    getProductFire(){
      //הוויליו מייצר את האובזווריבל
      return this.db.list('/products').valueChanges();
     }
  
    constructor(http:Http,private db:AngularFireDatabase) {

      this.http = http;}

}
