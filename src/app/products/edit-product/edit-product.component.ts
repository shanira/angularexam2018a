import { Component, OnInit , Output , EventEmitter } from '@angular/core';
import {FormGroup , FormControl, FormBuilder} from '@angular/forms';
import { ProductsService } from './../products.service';
import { ActivatedRoute, Router,ParamMap, Routes  } from '@angular/router';
import { HttpParams, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  name;
  price;

 //בניית ערוץ התקשורת בין message form to message
 @Output() addProduct:EventEmitter<any> = new EventEmitter<any>(); //any מגדיר סוג מסוים של מידע, במקרה הזה כל סוג
 @Output() addProductPs:EventEmitter<any> = new EventEmitter<any>(); //אירוע פסימי
id;
product;
service:ProductsService;

 //Reactive Form
  prdform = new FormGroup({
   name:new FormControl(),
   price:new FormControl(),
   id:new FormControl()
 });

   sendData(){
    this.addProduct.emit(this.prdform.value.name);
    this.prdform.value.id = this.id;
    
    this.service.updateProduct(this.prdform.value).subscribe(
      response =>{              
        this.addProductPs.emit();
        this.router.navigate(['/']);

      }

    )
  }

 constructor(service:ProductsService, private formBuilder:FormBuilder, private route: ActivatedRoute, private router: Router) {         
    this.service = service;    
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = +params.get('id'); // This line converts id from string into num      
      this.service.getProduct(this.id).subscribe(response=>{
        this.product = response.json(); 
        console.log(this.product.name);
        this.name = this.product.name
        this.price = this.product.price                                 
      });      
    });
  }


 ngOnInit() {
 }

}

