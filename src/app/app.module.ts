import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { ProductsService } from './products/products.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SearchResultComponent } from './search-result/search-result.component';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { FproductsComponent } from './products/fproducts/fproducts.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from './../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    LoginComponent,
    NotFoundComponent,
    NavigationComponent,
    SearchResultComponent,
    EditProductComponent,
    FproductsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
     {path: '', component: ProductsComponent},
     {path: 'login', component: LoginComponent},
     {path: 'search-result', component: SearchResultComponent},
     {path: 'edit-product/:id', component: EditProductComponent},
     {path: 'fproducts', component: FproductsComponent},
     {path: '**', component: NotFoundComponent}
      ])

  ],
  providers: [ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
