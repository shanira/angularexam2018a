// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url: 'http://localhost/angular/slim/',
  firebase:{
    apiKey: "AIzaSyDO65zv7rGW4Vs5z8jpeBZHKExssddywhQ",
    authDomain: "products-a1826.firebaseapp.com",
    databaseURL: "https://products-a1826.firebaseio.com",
    projectId: "products-a1826",
    storageBucket: "products-a1826.appspot.com",
    messagingSenderId: "60636079431"

  }
};


